# firstapp

A new Flutter project.

## Getting Started

Run app

``flutter run``

Install package:

``flutter pub get``

Create a stateful widget

Type stful and enter to use snippet for create a stateful widget, after this, type the name of state.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
